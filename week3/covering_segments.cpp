#include <algorithm>
#include <iostream>
#include <climits>
#include <vector>
#include <sstream>
#include <cmath>
#include <map>

using std::vector;

struct Segment {
    int start, end;

    friend bool operator<(Segment const &lhs, Segment const &rhs)
    {
        if ((lhs.start < rhs.start) && (lhs.end < rhs.end))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
};

bool by_start(Segment a, Segment b)
{
    return (a.start < b.start);
}

bool by_end(Segment a, Segment b)
{
    return (a.end < b.end);
}

vector<int> optimal_points(vector<Segment> segments)
{
    sort(segments.begin(), segments.end(), by_end);
    vector<int> points;
    int last_point = 0;

    for (size_t i = 0; i < segments.size(); ++i)
    {
        if ((segments[i].start > last_point) && (segments[i].end > last_point))
        {
            last_point = segments[i].end;
            points.push_back(last_point);
        }
    }
    return points;
}

#ifdef RUN_TESTS

struct TestCase {
    vector<Segment> segments;
    vector<int> expected;
};

template <class T>
std::string vec_str(vector<T> vec)
{
    std::stringstream ss;
    bool middle = false;
    for (auto thing : vec)
    {
        if (middle)
        {
            ss << ",";
        }
        ss << thing;
        middle = true;
    }
    return ss.str();
}

std::string segments_str(vector<Segment> vec)
{
    std::vector<std::string> segment_strings;
    std::vector<Segment>::iterator iter;
    for (iter = vec.begin(); iter != vec.end(); ++iter)
    {
        Segment seg = (*iter);
        std::stringstream ss;
        ss << "[" << seg.start << "," << seg.end << "]";
        segment_strings.push_back(ss.str());
    }

    return vec_str<std::string>(segment_strings);
}

vector<int> naive_covering_points(vector<Segment> segments)
{
    sort(segments.begin(), segments.end(), by_start);
    vector<Segment>::iterator iter;
    vector<int> points;
    std::map<int, int> points_to_count;
    std::map<Segment, vector<int>> seg_to_points;
    std::map<vector<int>, Segment> point_to_segs;
    for (auto seg : segments)
    {
        // points_to_count[seg.start]++;
        // points_to_count[seg.end]++;
        points.push_back(seg.start);
        points.push_back(seg.end);

        // seg_to_points[seg].push_back(seg.start);
        // seg_to_points[seg].push_back(seg.end);
    }
    for (auto point : points)
    {
        for (auto seg : segments)
        {
            if ((point >= seg.start) && (point <= seg.end))
            {
                points_to_count[point]++;

                seg_to_points[seg].push_back(point);
                point_to_segs[point].push_back(seg);
            }
        }
    }

    for (auto s_to_p : seg_to_points)
    {
        for (auto point : s_to_p.second)
        {

        }
    }

    vector<int> better;
    for (auto pc : points_to_count)
    {
        if (pc.second > 1)
        {
            better.push_back(pc.first);
        }
    }
    return better;
}

bool test_covering_segments(struct TestCase test_case, std::function <vector<int>(vector<Segment>)> func, std::string test_info)
{
    vector<int> results = func(test_case.segments);
    sort(results.begin(), results.end());
    sort(test_case.expected.begin(), test_case.expected.end());
    if (results != test_case.expected)
    {
        std::cout << "Test failed (" << test_info << "). segments=";
        std::cout << segments_str(test_case.segments) << " expected=";
        std::cout << vec_str(test_case.expected) << " results=";
        std::cout << vec_str(results) << std::endl;
    }
    return (results == test_case.expected);
}

bool test_covering_segments_num(struct TestCase test_case, std::function <vector<int>(vector<Segment>)> func, std::string test_info)
{
    vector<int> results = func(test_case.segments);
    sort(results.begin(), results.end());
    sort(test_case.expected.begin(), test_case.expected.end());
    if (results.size() != test_case.expected.size())
    {
        std::cout << "Test failed (" << test_info << "). segments=";
        std::cout << segments_str(test_case.segments) << " expected=";
        std::cout << vec_str(test_case.expected) << " results=";
        std::cout << vec_str(results) << std::endl;
    }
    return (results.size() == test_case.expected.size());
}

vector<Segment> gen_large_tests(int max)
{
    vector<Segment> generated;
    int num_of_segments = rand() % 100;

    for (int i = 0; i < num_of_segments; ++i)
    {
        generated.push_back({rand() % max, rand() % max});
    }

    return generated;
}

bool run_tests()
{
    int imax = std::numeric_limits<int>::max();
    double seg_max = std::pow(10.0, 9.0);
    long long smax = (long long)seg_max;
    std::cout << "imax=" << imax << std::endl;
    std::cout << "smax=" << smax << std::endl;

    std::vector<struct TestCase> test_cases;
    std::vector<struct TestCase>::iterator iter;
    test_cases.push_back({{{1, 3}, {2, 5}, {3, 6}}, {3}});
    test_cases.push_back({{{4, 7}, {1, 3}, {2, 5}, {5, 6}}, {3, 6}});
    test_cases.push_back({{{2, 4}, {3, 7}, {6, 8}}, {4, 8}});
    test_cases.push_back({{{1, 2}, {1, 3}, {1, 2}}, {2}});
    test_cases.push_back({{{2, 8}, {6, 11}, {13, 15}, {14, 18}, {16, 20}, {17, 21}}, {8, 15, 18}});

    for (iter = test_cases.begin(); iter != test_cases.end(); ++iter)
    {
        TestCase test_case = (*iter);
        if (!test_covering_segments(test_case, optimal_points, "optimal_points"))
        {
            return false;
        }
        if (!test_covering_segments_num(test_case, naive_covering_points, "naive_covering_points"))
        {
            return false;
        }
    }

    vector<Segment> generated_segments = gen_large_tests((int)smax);
    if (!test_covering_segments_num({generated_segments, naive_covering_points(generated_segments)}, optimal_points, "optimal_points"))
    {
        return false;
    }

    std::cout << "Tests passed.\n";
    return true;
}

#endif

int main()
{
#ifdef RUN_TESTS
    if (!run_tests())
    {
        return 1;
    }
#endif

    int n;
    std::cin >> n;
    vector<Segment> segments(n);
    for (size_t i = 0; i < segments.size(); ++i)
    {
        std::cin >> segments[i].start >> segments[i].end;
    }
    vector<int> points = optimal_points(segments);
    std::cout << points.size() << "\n";
    for (size_t i = 0; i < points.size(); ++i)
    {
        std::cout << points[i] << " ";
    }
}

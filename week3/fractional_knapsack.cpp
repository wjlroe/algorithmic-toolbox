#include <iostream>
#include <vector>
#include <sstream>
#include <tuple>
#include <algorithm>
#include <cmath>

using std::vector;
using std::tuple;

bool compare_value_tuple(tuple<int, double> a, tuple<int, double> b)
{
    return std::get<1>(a) < std::get<1>(b);
}

/*
    std::cout << "Sorted values:" << std::endl;
    std::vector<tuple<int, double>>::iterator iter;
    for (iter = sorted_values.begin(); iter != sorted_values.end(); ++iter)
    {
        std::cout.precision(10);
        std::cout << "(" << std::get<0>(*iter) << "," << std::get<1>(*iter) << ")" << std::endl;
    }
 */

double get_optimal_value(int capacity, vector<int> weights, vector<int> values)
{
    double total_value = 0.0;
    int num_weights = weights.size();
    vector<tuple<int, double>> sorted_values(num_weights);
    for (size_t i = 0; i < weights.size(); ++i)
    {
        double thing = (double)values[i] / weights[i];
        sorted_values[i] = std::make_tuple (i, thing);
    }
    sort(sorted_values.begin(), sorted_values.end(), compare_value_tuple);

    while (capacity > 0)
    {
        if (!sorted_values.empty())
        {
            // pick a thing
            tuple<int, double> highest_value = sorted_values.back();
            sorted_values.pop_back();
            int index = std::get<0>(highest_value);
            int weight = weights[index];
            // bump value
            int amount_to_use = std::min(weight, capacity);
            total_value += amount_to_use * std::get<1>(highest_value);
            // reduce capacity
            capacity -= amount_to_use;
        }
        else
        {
            // what to do when there isn't enough stuff?
            break;
        }
    }

    return total_value;
}

#ifdef RUN_TESTS

struct TestCase {
    int capacity;
    vector<int> values;
    vector<int> weights;
    double expected;
};

std::string vec_str(vector<int> vec)
{
    std::stringstream ss;
    std::vector<int>::iterator iter;
    for (iter = vec.begin(); iter != vec.end(); ++iter)
    {
        if (iter != vec.begin())
        {
            ss << ",";
        }
        ss << (*iter);
    }
    return ss.str();
}

bool double_eq(double a, double b)
{
    return (std::abs(a - b) < 0.0001);
}

bool test_knapsack(struct TestCase test_case)
{
    double result = get_optimal_value(test_case.capacity, test_case.weights, test_case.values);
    bool correct = double_eq(result, test_case.expected);
    if (!correct)
    {
        std::cout << "Test failed. capacity=" << test_case.capacity << " weights=";
        std::cout << vec_str(test_case.weights) << " values=" << vec_str(test_case.values);
        std::cout.precision(10);
        std::cout << " expected=" << test_case.expected << " but got=" << result << std::endl;
    }
    return correct;
}

bool run_tests()
{
    std::vector<struct TestCase> test_cases;
    std::vector<struct TestCase>::iterator iter;
    test_cases.push_back({50, {60, 100, 120}, {20, 50, 30}, 180.0});
    test_cases.push_back({10, {500}, {30}, 166.6667});
    test_cases.push_back({10, {}, {}, 0});
    test_cases.push_back({10, {5}, {5}, 5});
    for (iter = test_cases.begin(); iter != test_cases.end(); ++iter)
    {
        TestCase test_case = (*iter);
        if (!test_knapsack(test_case))
        {
            return false;
        }
    }
    std::cout << "Tests passed.\n";
    return true;
}
#endif

int main()
{

#ifdef RUN_TESTS
    if (!run_tests())
    {
        return 1;
    }
#endif

    int n;
    int capacity;
    std::cin >> n >> capacity;
    vector<int> values(n);
    vector<int> weights(n);
    for (int i = 0; i < n; i++)
    {
        std::cin >> values[i] >> weights[i];
    }

    double optimal_value = get_optimal_value(capacity, weights, values);

    std::cout.precision(10);
    std::cout << optimal_value << std::endl;
    return 0;
}

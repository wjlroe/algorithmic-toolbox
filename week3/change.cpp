#include <iostream>
#include <vector>

int get_change(int n)
{
    int remaining = n;
    int change = 0;
    while (remaining > 0)
    {
        if (remaining >= 10)
        {
            remaining -= 10;
            change++;
        }
        else if (remaining >= 5)
        {
            remaining -= 5;
            change++;
        }
        else
        {
            remaining--;
            change++;
        }
    }
    return change;
}

#ifdef RUN_TESTS
bool test_change(int input, int output)
{
    int result = get_change(input);
    if (result != output)
    {
        std::cout << "Test failed. input=" << input << " expected=" << output << " but got=" << result << '\n';
    }
    return (result == output);
}

struct TestCase {
    int input;
    int expected;
};

bool run_tests()
{
    std::vector<struct TestCase> test_cases;
    std::vector<struct TestCase>::iterator iter;
    test_cases.push_back({2, 2});
    test_cases.push_back({1, 1});
    test_cases.push_back({5, 1});
    test_cases.push_back({10, 1});
    test_cases.push_back({28, 6});
    for (iter = test_cases.begin(); iter != test_cases.end(); ++iter)
    {
        TestCase test_case = (*iter);
        if (!test_change(test_case.input, test_case.expected))
        {
            return false;
        }
    }
    std::cout << "Tests passed.\n";
    return true;
}
#endif

int main()
{

#ifdef RUN_TESTS
    if (!run_tests())
    {
        return 1;
    }
#endif

    int n;
    std::cin >> n;
    std::cout << get_change(n) << '\n';
}

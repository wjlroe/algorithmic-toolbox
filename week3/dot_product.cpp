#include <algorithm>
#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>
#include <cmath>

using std::vector;

long long naive_min_dot_product(vector<int> a, vector<int> b)
{
    long long min = 0;
    vector<int>::iterator a_iter;
    vector<int>::iterator b_iter;

    std::sort(a.begin(), a.end());
    std::sort(b.begin(), b.end());

    do {
        do {
            for (a_iter = a.begin(); a_iter < a.end(); ++a_iter)
            {
                for (b_iter = b.begin(); b_iter < b.end(); ++b_iter)
                {
                    if (a_iter == a.begin() && b_iter == b.begin())
                    {
                        min = (long long)(*a_iter) * (*b_iter);
                    }
                    else
                    {
                        min = std::min(min, ((long long)(*a_iter) * (*b_iter)));
                    }
                }
            }
        } while (std::next_permutation(b.begin(), b.end()));
    } while (std::next_permutation(a.begin(), a.end()));

    return min;
}

long long min_dot_product(vector<int> a, vector<int> b)
{
    long long result = 0;

    std::sort(a.begin(), a.end());
    std::sort(b.begin(), b.end());
    // reverse one so we are min/max-ing
    std::reverse(a.begin(), a.end());

    while (!a.empty())
    {
        int ai = a.back();
        a.pop_back();
        int bi = b.back();
        b.pop_back();
        result = result + (long long)ai * bi;
    }

    return result;
}

#ifdef RUN_TESTS

struct TestCase {
    vector<int> a;
    vector<int> b;
    long long expected;
};

std::string vec_str(vector<int> vec)
{
    std::stringstream ss;
    std::vector<int>::iterator iter;
    for (iter = vec.begin(); iter != vec.end(); ++iter)
    {
        if (iter != vec.begin())
        {
            ss << ",";
        }
        ss << (*iter);
    }
    return ss.str();
}

bool test_min_dot_product(struct TestCase test_case)
{
    long long result = min_dot_product(test_case.a, test_case.b);
    if (result != test_case.expected)
    {
        std::cout << "Test failed. a=";
        std::cout << vec_str(test_case.a) << " b=" << vec_str(test_case.b);
        std::cout << " expected=" << test_case.expected << " but got=" << result << std::endl;
    }
    return (result == test_case.expected);
}

int rand_int()
{
    int magnitude = rand() % 1000000;
    int positive = rand() % 2;
    if (positive == 0)
    {
        magnitude = -magnitude;
    }
    return magnitude;
}

void gen_test_cases(vector<struct TestCase> *test_cases, int size)
{
    int num = rand() % size;
    vector<int> as(num), bs(num);
    for (int j = 0; j < num; ++j)
    {
        as.push_back(rand_int());
        bs.push_back(rand_int());
    }

    test_cases->push_back({as, bs, naive_min_dot_product(as, bs)});
}

bool run_tests()
{
    std::vector<struct TestCase> test_cases;
    std::vector<struct TestCase>::iterator iter;
    test_cases.push_back({{23}, {39}, 897});
    test_cases.push_back({{1, 3, -5}, {-2, 4, 1}, -25});
    test_cases.push_back({{-2, 4, 1}, {1, 3, -5}, -25});
    test_cases.push_back({{}, {}, 0});

    for (int i = 0; i < 1; ++i)
    {
        gen_test_cases(&test_cases, 2);
        gen_test_cases(&test_cases, 10);
        gen_test_cases(&test_cases, 1000);
    }

    for (iter = test_cases.begin(); iter != test_cases.end(); ++iter)
    {
        TestCase test_case = (*iter);
        if (!test_min_dot_product(test_case))
        {
            return false;
        }
    }
    std::cout << "Tests passed.\n";
    return true;
}

#endif

int main()
{
#ifdef RUN_TESTS
    if (!run_tests())
    {
        return 1;
    }
#endif

    size_t n;
    std::cin >> n;
    vector<int> a(n), b(n);
    for (size_t i = 0; i < n; i++)
    {
        std::cin >> a[i];
    }
    for (size_t i = 0; i < n; i++)
    {
        std::cin >> b[i];
    }
    std::cout << min_dot_product(a, b) << std::endl;
}

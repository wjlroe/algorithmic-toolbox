CCFLAGS := -std=c++11 -g -Wall -Wextra -DRUN_TESTS
.SUFFIXES: .exe .cpp

.PHONEY: clean

EXERCISES := \
	week1/APlusB.cpp \
	week1/MaxPairwiseProduct.cpp \
	week2/fib.cpp \
	week2/fibonacci_last_digit.cpp \
	week2/gcd.cpp \
	week2/lcm.cpp \
	week2/fibonacci_huge.cpp \
	week3/change.cpp \
	week3/fractional_knapsack.cpp \
	week3/dot_product.cpp \
	week3/covering_segments.cpp

all: exes

exes: $(EXERCISES:.cpp=.exe)

.cpp.exe:
	@$(CXX) $(CCFLAGS) -o $@ $<

clean:
	@rm -rf **/*.exe **/*.dSYM

#include <iostream>

int calc_fib_fast(int n)
{
    if (n <= 1) {
        return n;
    }

    int Fk = 0;
    int Fj = 1;
    for (int j = 2; j <= n; ++j)
    {
        int Fjk = Fj + Fk;
        Fk = Fj;
        Fj = Fjk;
    }

    return Fj;
}

int calc_fib(int n)
{
    if (n <= 1)
    {
        return n;
    }

    return calc_fib(n - 1) + calc_fib(n - 2);
}

int test_fibs()
{
    for (int i = 0; i < 45; ++i)
    {
        int r1 = calc_fib(i);
        int r2 = calc_fib_fast(i);
        if (r1 != r2)
        {
            std::cout << "Results differ. naive: " << r1 << " fast: " << r2 << '\n';
            return 1;
        }
    }
    return 0;
}

int main()
{
    int n = 0;

    // int test_results = test_fibs();
    // if (test_results != 0)
    // {
    //     return 1;
    // }

    // std::cout << "Tests passed.\n";

    std::cin >> n;

    std::cout << calc_fib_fast(n) << '\n';
    return 0;
}

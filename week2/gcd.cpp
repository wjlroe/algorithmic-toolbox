#include <iostream>

int gcd_fast(int a, int b)
{
    while (b != 0)
    {
        int t = b;
        b = a % b;
        a = t;
    }
    return a;
}

int gcd_slow(int a, int b)
{
    int current_gcd = 1;
    for (int d = 2; d <= a && d <= b; d++)
    {
        if (a % d == 0 && b % d == 0)
        {
            if (d > current_gcd)
            {
                current_gcd = d;
            }
        }
    }
    return current_gcd;
}

int random_tests()
{
    for (int i = 0; i < 100; ++i)
    {
        int n1 = rand() % 1000000000;
        int n2 = rand() % 1000000000;
        int r1 = gcd_slow(n1, n2);
        int r2 = gcd_fast(n1, n2);
        if (r1 != r2)
        {
            std::cout << "Results differ for (" << n1 << "," << n2 << "). r1 = " << r1 << " r2 = " << r2 << '\n';
            return 1;
        }
    }
    return 0;
}

int main()
{
    // int test_result = random_tests();
    // if (test_result != 0)
    // {
    //     return 1;
    // } else {
    //     std::cout << "Tests passed.\n";
    // }

    int a, b;
    std::cin >> a >> b;
    std::cout << gcd_fast(a, b) << std::endl;
    return 0;
}

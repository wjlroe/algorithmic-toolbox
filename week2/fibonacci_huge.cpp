#include <iostream>
#include <vector>

int calc_fib_fast(int n)
{
    if (n <= 1) {
        return n;
    }

    int Fk = 0;
    int Fj = 1;
    for (int j = 2; j <= n; ++j)
    {
        int Fjk = Fj + Fk;
        Fk = Fj;
        Fj = Fjk;
    }

    return Fj;
}

long long naive_fibonaccihuge(long long n, long long m)
{
    return (calc_fib_fast(n) % m);
}

long long get_fibonaccihuge(long long n, long long m)
{
    std::vector<long long> mods;
    mods.push_back(0);
    mods.push_back(1);
    bool found_cycle = false;
    while (found_cycle != true)
    {
        long long next_mod = (mods[mods.size() - 2] +
                              mods[mods.size() - 1]) % m;
        mods.push_back(next_mod);

        if ((mods[mods.size() - 2] == 0) &&
            (mods[mods.size() - 1] == 1))
        {
            found_cycle = true;
        }
    }
    long long cycle_length = mods.size() - 2;
    return mods[n % cycle_length];
}

int main() {
    // for (long long i = 2; i < 16; ++i)
    // {
    //     for (long long m = 2; m < 4; ++m)
    //     {
    //         long long r1 = naive_fibonaccihuge(i, m);
    //         long long r2 = get_fibonaccihuge(i, m);
    //         if (r1 != r2)
    //         {
    //             std::cout << "Results differ! i=" << i << " m=" << m << " r1=" << r1 << " r2=" << r2 << '\n';
    //             return 1;
    //         }
    //     }
    // }

    // long long test_fib, test_mod;
    // test_fib = 281621358815590;
    // test_mod = 30524;
    // long long test_result = get_fibonaccihuge(test_fib, test_mod);
    // if (test_result == 11963)
    // {
    //     std::cout << "Test passed";
    // } else {
    //     std::cout << "Test failed. Got " << test_result << " instead of 11963!" << '\n';
    //     return 1;
    // }

    long long n, m;
    std::cin >> n >> m;
    std::cout << get_fibonaccihuge(n, m) << '\n';
}

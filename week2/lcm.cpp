#include <iostream>

int gcd_fast(int a, int b)
{
    while (b != 0)
    {
        int t = b;
        b = a % b;
        a = t;
    }
    return a;
}

long long lcm(int a, int b)
{
    return (long long)a * b / gcd_fast(a, b);
}

int main()
{
    int a, b;
    std::cin >> a >> b;
    std::cout << lcm(a, b) << std::endl;
    return 0;
}

#include <iostream>

int get_fib_last_digit_fast(int n)
{
    if (n <= 1)
    {
        return n;
    }

    int Fk = 0;
    int Fj = 1;
    for (int j = 2; j <= n; ++j)
    {
        int Fjk = (Fj + Fk) % 10;
        Fk = Fj;
        Fj = Fjk;
    }

    return Fj;
}

int get_fib_last_digit(int n)
{
    if (n <= 1)
    {
        return n;
    }
    return (get_fib_last_digit(n - 1) + get_fib_last_digit(n - 2)) % 10;
}

int test_them()
{
    for (int i = 0; i < 20; ++i)
    {
        int r1 = get_fib_last_digit(i);
        int r2 = get_fib_last_digit_fast(i);
        if (r1 != r2)
        {
            std::cout << "Results differ for i = " << i << ". naive: " << r1 << " fast: " << r2 << '\n';
            return 1;
        }
    }

    std::cout << "Tests passed.\n";

    return 0;
}

int main()
{
    //test_them();

    int n;
    std::cin >> n;
    int c = get_fib_last_digit_fast(n);
    std::cout << c << '\n';
}

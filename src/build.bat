@echo off

call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64

for %%w in (week1 week2 week3) do (
    pushd %%w
    for %%i in (*.cpp) do cl -EHsc -Zi %%i
    popd
)
